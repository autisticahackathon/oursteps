angular.module('app.controllers', [])
  
.controller('splashCtrl', ['$scope', '$stateParams', // The following is the constructor function for this page's controller. See https://docs.angularjs.org/guide/controller
// You can include any angular dependencies as parameters for this function
// TIP: Access Route Parameters for your page via $stateParams.parameterName
function ($scope, $stateParams) {


}])
   
.controller('parentCtrl', ['$scope', '$stateParams', // The following is the constructor function for this page's controller. See https://docs.angularjs.org/guide/controller
// You can include any angular dependencies as parameters for this function
// TIP: Access Route Parameters for your page via $stateParams.parameterName
function ($scope, $stateParams) {


}])
   
.controller('parent2Ctrl', ['$scope', '$stateParams', // The following is the constructor function for this page's controller. See https://docs.angularjs.org/guide/controller
// You can include any angular dependencies as parameters for this function
// TIP: Access Route Parameters for your page via $stateParams.parameterName
function ($scope, $stateParams) {


}])
   
.controller('childCtrl', ['$scope', '$stateParams', // The following is the constructor function for this page's controller. See https://docs.angularjs.org/guide/controller
// You can include any angular dependencies as parameters for this function
// TIP: Access Route Parameters for your page via $stateParams.parameterName
function ($scope, $stateParams) {


}])
   
.controller('carCtrl', ['$scope', '$stateParams', '$state', // The following is the constructor function for this page's controller. See https://docs.angularjs.org/guide/controller
// You can include any angular dependencies as parameters for this function
// TIP: Access Route Parameters for your page via $stateParams.parameterName
function ($scope, $stateParams, $state) {

        $scope.onSwipeGoTo = function(id) {
            $state.go(id, { })
        };

        $scope.imageUrl = 'img/temp_level1.png';
        $scope.tapDirection = 'UP';

        $scope.increase = function() {
            switch($scope.imageUrl) {
                case 'img/temp_level1.png':
                    $scope.imageUrl = 'img/temp_level2.png';
                    break;
                case 'img/temp_level2.png':
                    $scope.imageUrl = 'img/temp_level3.png';
                    break;
                case 'img/temp_level3.png':
                    $scope.imageUrl = 'img/temp_level4.png';
                    break;
                case 'img/temp_level4.png':
                    $scope.imageUrl = 'img/temp_level5.png';
                    break;
                case 'img/temp_level5.png':
                default:
            }
        };

        $scope.decrease = function() {
            switch($scope.imageUrl) {
                case 'img/temp_level1.png':
                    break;
                case 'img/temp_level2.png':
                    $scope.imageUrl = 'img/temp_level1.png';
                    break;
                case 'img/temp_level3.png':
                    $scope.imageUrl = 'img/temp_level2.png';
                    break;
                case 'img/temp_level4.png':
                    $scope.imageUrl = 'img/temp_level3.png';
                    break;
                case 'img/temp_level5.png':
                    $scope.imageUrl = 'img/temp_level4.png';
                    break;
                default:
            }
        };

    $scope.tap = function() {
        if ($scope.tapDirection == 'UP') {
            if ($scope.imageUrl == 'img/temp_level5.png') {
                $scope.tapDirection = 'DOWN';
                $scope.decrease();
            } else {
                $scope.increase();
            }
        } else {
            if ($scope.imageUrl == 'img/temp_level1.png') {
                $scope.tapDirection = 'UP';
                $scope.increase();
            } else {
                $scope.decrease();
            }
        }
    };

}])
   
.controller('signCtrl', ['$scope', '$stateParams', '$state', // The following is the constructor function for this page's controller. See https://docs.angularjs.org/guide/controller
// You can include any angular dependencies as parameters for this function
// TIP: Access Route Parameters for your page via $stateParams.parameterName
function ($scope, $stateParams, $state) {

    $scope.onSwipeGoTo = function(id) {
        $state.go(id, { })
    };

    $scope.imageUrl = 'img/temp_level1.png';
    $scope.tapDirection = 'UP';

    $scope.increase = function() {
        switch($scope.imageUrl) {
            case 'img/temp_level1.png':
                $scope.imageUrl = 'img/temp_level2.png';
                break;
            case 'img/temp_level2.png':
                $scope.imageUrl = 'img/temp_level3.png';
                break;
            case 'img/temp_level3.png':
                $scope.imageUrl = 'img/temp_level4.png';
                break;
            case 'img/temp_level4.png':
                $scope.imageUrl = 'img/temp_level5.png';
                break;
            case 'img/temp_level5.png':
            default:
        }
    };

    $scope.decrease = function() {
        switch($scope.imageUrl) {
            case 'img/temp_level1.png':
                break;
            case 'img/temp_level2.png':
                $scope.imageUrl = 'img/temp_level1.png';
                break;
            case 'img/temp_level3.png':
                $scope.imageUrl = 'img/temp_level2.png';
                break;
            case 'img/temp_level4.png':
                $scope.imageUrl = 'img/temp_level3.png';
                break;
            case 'img/temp_level5.png':
                $scope.imageUrl = 'img/temp_level4.png';
                break;
            default:
        }
    };

    $scope.tap = function() {
        if ($scope.tapDirection == 'UP') {
            if ($scope.imageUrl == 'img/temp_level5.png') {
                $scope.tapDirection = 'DOWN';
                $scope.decrease();
            } else {
                $scope.increase();
            }
        } else {
            if ($scope.imageUrl == 'img/temp_level1.png') {
                $scope.tapDirection = 'UP';
                $scope.increase();
            } else {
                $scope.decrease();
            }
        }
    };
}])
   
.controller('schoolCtrl', ['$scope', '$stateParams', '$state', // The following is the constructor function for this page's controller. See https://docs.angularjs.org/guide/controller
// You can include any angular dependencies as parameters for this function
// TIP: Access Route Parameters for your page via $stateParams.parameterName
function ($scope, $stateParams, $state) {

    $scope.onSwipeGoTo = function(id) {
        $state.go(id, { })
    };

    $scope.imageUrl = 'img/temp_level1.png';
    $scope.tapDirection = 'UP';

    $scope.increase = function() {
        switch($scope.imageUrl) {
            case 'img/temp_level1.png':
                $scope.imageUrl = 'img/temp_level2.png';
                break;
            case 'img/temp_level2.png':
                $scope.imageUrl = 'img/temp_level3.png';
                break;
            case 'img/temp_level3.png':
                $scope.imageUrl = 'img/temp_level4.png';
                break;
            case 'img/temp_level4.png':
                $scope.imageUrl = 'img/temp_level5.png';
                break;
            case 'img/temp_level5.png':
            default:
        }
    };

    $scope.decrease = function() {
        switch($scope.imageUrl) {
            case 'img/temp_level1.png':
                break;
            case 'img/temp_level2.png':
                $scope.imageUrl = 'img/temp_level1.png';
                break;
            case 'img/temp_level3.png':
                $scope.imageUrl = 'img/temp_level2.png';
                break;
            case 'img/temp_level4.png':
                $scope.imageUrl = 'img/temp_level3.png';
                break;
            case 'img/temp_level5.png':
                $scope.imageUrl = 'img/temp_level4.png';
                break;
            default:
        }
    };

    $scope.tap = function() {
        if ($scope.tapDirection == 'UP') {
            if ($scope.imageUrl == 'img/temp_level5.png') {
                $scope.tapDirection = 'DOWN';
                $scope.decrease();
            } else {
                $scope.increase();
            }
        } else {
            if ($scope.imageUrl == 'img/temp_level1.png') {
                $scope.tapDirection = 'UP';
                $scope.increase();
            } else {
                $scope.decrease();
            }
        }
    };
}])
   
.controller('homeCtrl', ['$scope', '$stateParams', '$state', // The following is the constructor function for this page's controller. See https://docs.angularjs.org/guide/controller
// You can include any angular dependencies as parameters for this function
// TIP: Access Route Parameters for your page via $stateParams.parameterName
function ($scope, $stateParams, $state) {

    $scope.onSwipeGoTo = function(id) {
        $state.go(id, { })
    };

    $scope.imageUrl = 'img/temp_level1.png';
    $scope.tapDirection = 'UP';

    $scope.increase = function() {
        switch($scope.imageUrl) {
            case 'img/temp_level1.png':
                $scope.imageUrl = 'img/temp_level2.png';
                break;
            case 'img/temp_level2.png':
                $scope.imageUrl = 'img/temp_level3.png';
                break;
            case 'img/temp_level3.png':
                $scope.imageUrl = 'img/temp_level4.png';
                break;
            case 'img/temp_level4.png':
                $scope.imageUrl = 'img/temp_level5.png';
                break;
            case 'img/temp_level5.png':
            default:
        }
    };

    $scope.decrease = function() {
        switch($scope.imageUrl) {
            case 'img/temp_level1.png':
                break;
            case 'img/temp_level2.png':
                $scope.imageUrl = 'img/temp_level1.png';
                break;
            case 'img/temp_level3.png':
                $scope.imageUrl = 'img/temp_level2.png';
                break;
            case 'img/temp_level4.png':
                $scope.imageUrl = 'img/temp_level3.png';
                break;
            case 'img/temp_level5.png':
                $scope.imageUrl = 'img/temp_level4.png';
                break;
            default:
        }
    };

    $scope.tap = function() {
        if ($scope.tapDirection == 'UP') {
            if ($scope.imageUrl == 'img/temp_level5.png') {
                $scope.tapDirection = 'DOWN';
                $scope.decrease();
            } else {
                $scope.increase();
            }
        } else {
            if ($scope.imageUrl == 'img/temp_level1.png') {
                $scope.tapDirection = 'UP';
                $scope.increase();
            } else {
                $scope.decrease();
            }
        }
    };
}])
   
.controller('createStoryCtrl', ['$scope', '$stateParams', // The following is the constructor function for this page's controller. See https://docs.angularjs.org/guide/controller
// You can include any angular dependencies as parameters for this function
// TIP: Access Route Parameters for your page via $stateParams.parameterName
function ($scope, $stateParams) {


}])
   
.controller('storyItemCtrl', ['$scope', '$stateParams', // The following is the constructor function for this page's controller. See https://docs.angularjs.org/guide/controller
// You can include any angular dependencies as parameters for this function
// TIP: Access Route Parameters for your page via $stateParams.parameterName
function ($scope, $stateParams) {


}])
   
.controller('pictureSelectorCtrl', ['$scope', '$stateParams', // The following is the constructor function for this page's controller. See https://docs.angularjs.org/guide/controller
// You can include any angular dependencies as parameters for this function
// TIP: Access Route Parameters for your page via $stateParams.parameterName
function ($scope, $stateParams) {


}])
   
.controller('feelingsCtrl', ['$scope', '$stateParams', // The following is the constructor function for this page's controller. See https://docs.angularjs.org/guide/controller
// You can include any angular dependencies as parameters for this function
// TIP: Access Route Parameters for your page via $stateParams.parameterName
function ($scope, $stateParams) {

    var emotions = ["great", "good", "okay", "bad", "really bad"];
    var location = ["school", "home", "public place", "other"];
    var badList = ["nervous", "angry", "scared", "sad"];
    var scale = [1, 2, 3, 4, 5];

    $scope.feelingsText = "Hi, this is the OurSteps Application.\n\nMaybe you would like to discuss your feelings?\nType your answers below and I will do my best to help you.";

    $scope.thingsToDo = function() {
        document.writeln("Here is a list of activities you could do:");
        document.writeln("- Go for a walk. Getting fresh air will be good for your system.");
        document.writeln("- Maybe have a go at meditating, that will help to calm you down.");
        document.writeln("- Try and talk to someone you trust.")
    };





    }])
 