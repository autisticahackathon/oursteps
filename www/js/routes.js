angular.module('app.routes', [])

.config(function($stateProvider, $urlRouterProvider) {

  // Ionic uses AngularUI Router which uses the concept of states
  // Learn more here: https://github.com/angular-ui/ui-router
  // Set up the various states which the app can be in.
  // Each state's controller can be found in controllers.js
  $stateProvider
    
  

      .state('splash', {
    url: '/page1',
    templateUrl: 'templates/splash.html',
    controller: 'splashCtrl'
  })

  .state('parent', {
    url: '/page2',
    templateUrl: 'templates/parent.html',
    controller: 'parentCtrl'
  })

  .state('parent2', {
    url: '/page11',
    templateUrl: 'templates/parent2.html',
    controller: 'parent2Ctrl'
  })

  .state('child', {
    url: '/page4',
    templateUrl: 'templates/child.html',
    controller: 'childCtrl'
  })

  .state('car', {
    url: '/page5',
    templateUrl: 'templates/car.html',
    controller: 'carCtrl'
  })

  .state('sign', {
    url: '/page7',
    templateUrl: 'templates/sign.html',
    controller: 'signCtrl'
  })

  .state('school', {
    url: '/page6',
    templateUrl: 'templates/school.html',
    controller: 'schoolCtrl'
  })

  .state('home', {
    url: '/page8',
    templateUrl: 'templates/home.html',
    controller: 'homeCtrl'
  })

  .state('createStory', {
    url: '/page9',
    templateUrl: 'templates/createStory.html',
    controller: 'createStoryCtrl'
  })

  .state('storyItem', {
    url: '/page10',
    templateUrl: 'templates/storyItem.html',
    controller: 'storyItemCtrl'
  })

  .state('pictureSelector', {
    url: '/page13',
    templateUrl: 'templates/pictureSelector.html',
    controller: 'pictureSelectorCtrl'
  })

  .state('feelings', {
    url: '/page14',
    templateUrl: 'templates/feelings.html',
    controller: 'feelingsCtrl'
  })

$urlRouterProvider.otherwise('/page1')

  

});