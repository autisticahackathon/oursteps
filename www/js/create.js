var lastChangedAt;
var photoEditing = null;
var mouseElm;


function changeBackground(elm) {
  photoEditing = elm;
  pickPhoto(["img/house.jpg", "img/park.gif", "img/school.jpg", "img/school-bus.jpg", "img/dog.jpg", "img/girl.jpg", "img/girl2.jpg "]);
}


function changeTextContent(div) {
  var text = prompt("Enter new text");
  if (text != null) {
    div.innerHTML = text;
  }
  lastChangedAt = Date.now(); 
}

function addTopImage() {
  pickPhoto(["img/house.jpg", "img/park.gif", "img/school.jpg", "img/school-bus.jpg", "img/dog.jpeg", "img/girl.jpg", "img/girl2.jpg"]);
}

function addTopText() {
  newTop = document.createElement("div");
  newTop.classList.add("draggable");
  newTop.classList.add("topText");
  /*newTop.onmouseover = function() { mouseElm = this; }*/
  newTop.innerHTML = "Double-click to edit";
  document.getElementById("scene").appendChild(newTop);
  var children = document.getElementById("scene").children;
  loadDrag(children[children.length-1]);
}

function loadDrag(draggableElem) {
  /*var draggableElems = document.querySelectorAll('.draggable');
  // array of Draggabillies
  var draggies = [];
  // init Draggabillies
  for ( var i=0, len = draggableElems.length; i < len; i++ ) {
    var draggableElem = draggableElems[i];*/
    var draggie = new Draggabilly( draggableElem, {
      containment: true
    });
    draggie.on( 'staticClick', function() {
      if (this.element.tagName == "IMG") {
        changeBackground(this.element);
      }
      else {
        changeTextContent(this.element);
      }
    });
    //draggies.push( draggie );
  //}
  //console.log(draggies);
  ff()
}


function loadState() {
  document.getElementById("");
}
/*function saveScene() {
  var storeVar = [];
  storeVar.tops = [];
  storeVar.lefts = [];
  storeVar.types = [];
  storeVar.datas = [];
  var draggableElem = document.querySelectorAll('.draggable');
  for (var i = 0; i < draggableElem.length; i++) {
    storeVar.tops[i] = draggableElem[i].style.top;
    storeVar.lefts[i] = draggableElem[i].style.left;
    if (draggableElem[i].tagName == "IMG") {
      storeVar.types[i] = "img";
      storeVar.datas[i] = draggableElem[i].src;
    }
    else if (draggableElem[i].tagName == "DIV") {
      storeVar.types[i] = "div";
      storeVar.datas[i] = draggableElem[i].innerHTML;
    }
  }
  storeVar.backgroundImage = document.getElementById("scene").style.backgroundImage;
  return storeVar;
}

function loadScene(storeVar) {
  document.getElementById("scene").style.backgroundImage = storeVar.backgroundImage;
  for (var i = 0; i < storeVar.tops.length; i++) {
    var node = document.createElement(storeVar.types[i]);
    node.style.top = storeVar.tops[i];
    node.style.left = storeVar.lefts[i];
    node.style.position = "relative";
    if (storeVar.types[i] == "img") {
      node.src = storeVar.datas[i];
    }
    else if (storeVar.types[i] == "div") {
      node.backgroundImage = storeVar.datas[i];
      node.backgroundSize = "600px 400px";
    }
    document.getElementById("scene").appendChild(node.cloneNode(true));
  }
}*/

function photoPickCallback(newSrc) {
  if (newSrc != null) {
    if (photoEditing != null && photoEditing.tagName == "DIV") {
      alert(photoEditing);
      elm = photoEditing;
      photoEditing = null;
      elm.style.backgroundImage = "url('" + newSrc + "')";
      elm.style.backgroundSize = "600px 400px";
      saveState(newSrc,document.getElementById("texty").value );
    }
    else {
      newTop = document.createElement("img");
      newTop.src = newSrc;
      newTop.classList.add("draggable");
      document.getElementById("scene").appendChild(newTop);
      var children = document.getElementById("scene").children;
      loadDrag(children[children.length-1]);
    }
  }
  lastChangedAt = Date.now();
}

function saveState(srcy,txt) {
  console.log(srcy);
  try {
    console.log("okok");
    document.getElementById("imgS97").src = srcy;
    document.getElementById("text294").innerHTML = txt;
    console.log(document.getElementById("imgS97").src);
  }
  catch(err) {
    console.log("badbad");
  }
}

function ff() {
  document.getElementById("scene").onclick = function(e) {
    e = e || event
    var target = e.target || e.srcElement
    if (this.id == target.id) {
      changeBackground(this);
    }
  }
}
