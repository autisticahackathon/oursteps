function pickPhoto(arrayOfImages) {
  document.querySelectorAll('.scroll')[0].style.transform = "translate3d(0px, 0px, 0px) scale(1)"
  mainDiv = document.getElementById("mainDiv");
  pageWidth = mainDiv.offsetWidth;
  numColums = Math.floor(pageWidth/200);
  numRows = Math.ceil(arrayOfImages.length / numColums);

  mainDiv.style.display = "none";
  var photoPickContainer = document.createElement("div");
  photoPickContainer.id = "photoPickContainer";
  var header = document.createElement("p");
  photoPickContainer.innerHTML = photoPickContainer.innerHTML + "<button onclick='()' class='button button-stable ion-camera' style='width: 100%; font-size: 20pt;'></button><br /><p></p><label class='item item-input' style='width: 80%; float: left;'><input id='imgUrl' type='text' placeholder='http://www.example.com/photo.jpg'></label><button onclick='urlReturn()' class='button button-stable' style='width: 20%;'>Use</button><br />";
  var subheading = document.createElement("h5");
  subheading.innerHTML = "Or select an image below:";
  photoPickContainer.appendChild(subheading);
  var photoTable = document.createElement("table");
  photoTable.style.padding = "50px";
  var currentCell = 0;
  tableLoop:
  for (var i = 0; i < numRows; i++) {
    var photoRow = document.createElement("tr");
    for (var i = 0; i < numColums; i++) {
      var img = document.createElement("img");
      img.src = arrayOfImages[currentCell];
      img.style.width = "15%";
      img.onclick = function() {
        var photoPickContainer = document.getElementById("photoPickContainer");
        photoPickContainer.parentNode.removeChild(photoPickContainer);
        document.getElementById("mainDiv").style.display = "block";
        document.querySelectorAll('.scroll')[0].style.transform = "translate3d(0px, 0px, 0px) scale(1)"
        photoPickCallback(this.src);
      }
      var newTmpNode = img.cloneNode(true);
      newTmpNode.onclick = img.onclick;
      photoPickContainer.appendChild(newTmpNode);
      currentCell++;
      if (currentCell == arrayOfImages.length) {
        break tableLoop;
      }
    }
    photoTable.appendChild(photoRow);
  }
  mainDiv.parentNode.appendChild(photoPickContainer);
}

function urlReturn() {
  url = document.getElementById("imgUrl").value;
  var photoPickContainer = document.getElementById("photoPickContainer");
  photoPickContainer.parentNode.removeChild(photoPickContainer);
  document.getElementById("mainDiv").style.display = "block";
  document.querySelectorAll('.scroll')[0].style.transform = "translate3d(0px, 0px, 0px) scale(1)"
  photoPickCallback(url);
}
